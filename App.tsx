import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import LoginScreen from './src/screens/LoginScreen';
import { UserProvider } from './src/hooks/useUser';
import UserRegistrationScreen from './src/screens/User/UserRegistrationScreen';
import AuthenticatedScreen from './src/screens/AuthenticatedScreens';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { ColorProvider } from './src/hooks/useColor';
import { AccountProvider } from './src/hooks/useAccount';
import { CategoryProvider } from './src/hooks/useCategory';
import { TransactionProvider } from './src/hooks/useTransaction';

const Stack = createNativeStackNavigator();

export default function App() {
	return (

		<SafeAreaProvider>
			<NavigationContainer>
				<UserProvider>
					<ColorProvider>
						<AccountProvider>
							<CategoryProvider>
								<TransactionProvider>
									<Stack.Navigator screenOptions={{ headerShown: false }}>
										<Stack.Screen name="Login" component={LoginScreen} />
										<Stack.Screen name="UserRegistration" component={UserRegistrationScreen} />
										<Stack.Screen name="AuthenticatedScreen" component={AuthenticatedScreen} />
									</Stack.Navigator>
								</TransactionProvider>
							</CategoryProvider>
						</AccountProvider>
					</ColorProvider>
				</UserProvider>
			</NavigationContainer>
		</SafeAreaProvider>
	);
}

