import React, { createContext, ReactNode, useContext, useEffect, useState } from "react";
import { api } from "../services/api";
import AsyncStorage from '@react-native-async-storage/async-storage';

type User = {
	id: number;
	name: string;
	email: string;
}

type Token = {
	headers: {
		Authorization: string;
	}
}

type UserContextData = {
	user: User | null;
	signIn: (token: string, user: User) => void;
	signOut: () => void;
	token: Token | null;
}

export const UserContext = createContext({} as UserContextData)

type UserProvider = {
	children: ReactNode;
}

export function UserProvider(props: UserProvider) {
	const [user, setUser] = useState<User | null>(null)
	const [token, setToken] = useState<Token | null>(null)

	async function signIn(token: string, user: User) {
		await AsyncStorage.setItem('financappToken', token);
		setUser(user);
		setToken({ headers: { Authorization: `Bearer ${token}` } });
	}

	async function signOut() {
		setUser(null);
		setToken(null);
		await AsyncStorage.removeItem('financappToken')
	}

	async function getToken() {
		const token = await AsyncStorage.getItem('financappToken')
		setToken({ headers: { Authorization: `Bearer ${token}` } })
	}

	useEffect(() => {
		getToken()

		if (token) {

			api.get('/user/get').then(response => {
				setUser(response.data.message)
			}).catch((error) => {
				signOut()
			})
		}
	}, [])

	return (
		<UserContext.Provider value={{
			user,
			signIn,
			signOut,
			token
		}}>
			{props.children}
		</UserContext.Provider>
	)
}

export const useUser = () => (useContext(UserContext))