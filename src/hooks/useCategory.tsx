import React, { createContext, ReactNode, useContext, useEffect, useState } from "react";
import { api } from "../services/api";
import { useUser } from "./useUser";
import { Color } from "./useColor";

export type Category = {
	id: number;
	name: string;
	type: 'income' | 'outcome';
	color: Color;
	active: boolean;
}

type CategoryContextData = {
	categories: Category[],
	activeCategories: Category[],
	getCategories: () => void;
	openCategoryModal: (category?: Category) => void;
	closeCategoryModal: () => void;
	isOpenCategoryModal: boolean;
	editCategory: Category | null;
	deactivateCategory: (categoryId?: number) => void;
}

export const CategoryContext = createContext({} as CategoryContextData)

type CategoryProvider = {
	children: ReactNode;
}

export function CategoryProvider(props: CategoryProvider) {
	const { token } = useUser();

	const [categories, setCategories] = useState<Category[]>([])
	const [activeCategories, setActiveCategories] = useState<Category[]>([])
	const [editCategory, setEditCategory] = useState<Category | null>(null)
	const [isOpenCategoryModal, setIsOpenCategoryModal] = useState(false)

	async function getCategories() {
		await api.get('category/get/all', token || {}).then(response => {
			const sortedCategories = response.data.categories.sort((a: Category, b: Category) => (
				a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0)
			)
			setCategories(sortedCategories)
			setActiveCategories(sortedCategories.filter((item: Category) => item.active === true))
		}).catch((error) => {
			console.log(error)
		})
	}

	function openCategoryModal(category?: Category) {
		setEditCategory(category || null)
		setIsOpenCategoryModal(true)
	}

	function closeCategoryModal() {
		setEditCategory(null)
		getCategories()
		setIsOpenCategoryModal(false)
	}

	async function deactivateCategory(categoryId?: number) {
		const id = categoryId ? categoryId : editCategory?.id
		await api.delete(`category/delete/${id}`, token || {}).catch((error) => {
			console.log(error)
		})
		closeCategoryModal()
	}

	useEffect(() => {
		if (token) {
			getCategories();
		}
	}, [token])

	return (
		<CategoryContext.Provider value={{
			categories,
			activeCategories,
			getCategories,
			openCategoryModal,
			closeCategoryModal,
			isOpenCategoryModal,
			editCategory,
			deactivateCategory
		}}>
			{props.children}
		</CategoryContext.Provider>
	)
}

export const useCategory = () => useContext(CategoryContext);