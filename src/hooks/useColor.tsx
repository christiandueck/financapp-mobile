import React, { createContext, ReactNode, useContext, useEffect, useState } from "react";

export type Color = {
	id: number;
	name: string;
	hex_code: string;
}

type ColorContextData = {
	colors: Color[],
}

export const ColorContext = createContext({} as ColorContextData)

type ColorProvider = {
	children: ReactNode;
}

export function ColorProvider(props: ColorProvider) {
	const colors = [
		{
			"id": 1,
			"name": "blue",
			"hex_code": "#4267B2",
			"created_at": "2021-12-09T22:07:50.000000Z",
			"updated_at": "2021-12-09T22:07:50.000000Z"
		},
		{
			"id": 2,
			"name": "cyan",
			"hex_code": "#1DB4F5",
			"created_at": "2021-12-09T22:07:50.000000Z",
			"updated_at": "2021-12-09T22:07:50.000000Z"
		},
		{
			"id": 3,
			"name": "green",
			"hex_code": "#20B74A",
			"created_at": "2021-12-09T22:07:50.000000Z",
			"updated_at": "2021-12-09T22:07:50.000000Z"
		},
		{
			"id": 4,
			"name": "yellow",
			"hex_code": "#9DB410",
			"created_at": "2021-12-09T22:07:50.000000Z",
			"updated_at": "2021-12-09T22:07:50.000000Z"
		},
		{
			"id": 5,
			"name": "orange",
			"hex_code": "#F5781D",
			"created_at": "2021-12-09T22:07:50.000000Z",
			"updated_at": "2021-12-09T22:07:50.000000Z"
		},
		{
			"id": 6,
			"name": "red",
			"hex_code": "#F51D1D",
			"created_at": "2021-12-09T22:07:50.000000Z",
			"updated_at": "2021-12-09T22:07:50.000000Z"
		},
		{
			"id": 7,
			"name": "violet",
			"hex_code": "#961DF5",
			"created_at": "2021-12-09T22:07:50.000000Z",
			"updated_at": "2021-12-09T22:07:50.000000Z"
		},
		{
			"id": 8,
			"name": "light-violet",
			"hex_code": "#C289EF",
			"created_at": "2021-12-09T22:07:50.000000Z",
			"updated_at": "2021-12-09T22:07:50.000000Z"
		}
	]

	return (
		<ColorContext.Provider value={{ colors }}>
			{props.children}
		</ColorContext.Provider>
	)
}

export const useColor = () => useContext(ColorContext);