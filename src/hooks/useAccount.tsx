import React, { createContext, ReactNode, useContext, useEffect, useState } from "react";
import { api } from "../services/api";
import { Color } from "./useColor";
import { useUser } from "./useUser";

export type Account = {
	id: number;
	name: string;
	type: 'bank';
	color: Color;
	balance: number;
	invoice_closing_date: number;
	invoice_due_date: number;
	active: boolean;
}

type AccountContextData = {
	accounts: Account[],
	activeAccounts: Account[],
	getAccounts: () => void;
	openAccountModal: (account?: Account) => void;
	closeAccountModal: () => void;
	isOpenAccountModal: boolean;
	editAccount: Account | null;
	deactivateAccount: (AccountId?: number) => void;
}

export const AccountContext = createContext({} as AccountContextData)

type AccountProvider = {
	children: ReactNode;
}

export function AccountProvider(props: AccountProvider) {
	const { token } = useUser();

	const [accounts, setAccounts] = useState<Account[]>([])
	const [activeAccounts, setActiveAccounts] = useState<Account[]>([])
	const [editAccount, setEditAccount] = useState<Account | null>(null)
	const [isOpenAccountModal, setIsOpenAccountModal] = useState(false)

	async function getAccounts() {
		await api.get('account/get/all', token || {}).then(response => {
			const sortedAccounts = response.data?.accounts.sort((a: Account, b: Account) => (
				a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0)
			)
			setAccounts(sortedAccounts)
			setActiveAccounts(sortedAccounts.filter((item: Account) => (item.active === true)))
		}).catch((error) => {
			console.log(error)
		})
	}

	function openAccountModal(account?: Account) {
		setEditAccount(account || null)
		setIsOpenAccountModal(true)
	}

	function closeAccountModal() {
		setEditAccount(null)
		getAccounts()
		setIsOpenAccountModal(false)
	}

	async function deactivateAccount(AccountId?: number) {
		const id = AccountId ? AccountId : editAccount?.id
		await api.delete(`account/delete/${id}`, token || {}).catch((error) => {
			console.log(error)
		})
		closeAccountModal()
	}

	useEffect(() => {
		if (token) {
			getAccounts();
		}
	}, [token])

	return (
		<AccountContext.Provider value={{
			accounts,
			activeAccounts,
			getAccounts,
			openAccountModal,
			closeAccountModal,
			isOpenAccountModal,
			editAccount,
			deactivateAccount
		}}>
			{props.children}
		</AccountContext.Provider>
	)
}

export const useAccount = () => useContext(AccountContext);