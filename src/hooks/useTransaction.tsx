import React, { createContext, ReactNode, useContext, useEffect, useState } from "react";
import { api } from "../services/api";
import { Account, useAccount } from "./useAccount";
import { Category } from "./useCategory";
import { useUser } from "./useUser";

export type Transaction = {
	id: number;
	description: string;
	type: 'income' | 'outcome';
	amount: number;
	date: string;
	category: Category;
	account: Account;
	installment: string;
}

type Period = {
	year: number;
	month: number;
}

type PaymentSummary = {
	period_incomes: number;
	period_outcomes: number;
	period_balance: number;
}

type TransactionContextData = {
	transactions: Transaction[],
	getTransactions: (month: string) => void;
	openTransactionModal: (transaction?: Transaction) => void;
	closeTransactionModal: () => void;
	isOpenTransactionModal: boolean;
	editTransaction: Transaction | null;
	deleteTransaction: (TransactionId?: number) => void;
	period: Period;
	setPeriod: (period: Period) => void;
	summary: PaymentSummary;
}

export const TransactionContext = createContext({} as TransactionContextData)

type TransactionProvider = {
	children: ReactNode;
}

export function TransactionProvider(props: TransactionProvider) {
	const { token } = useUser();
	const { getAccounts } = useAccount();

	const [transactions, setTransactions] = useState<Transaction[]>([])
	const [summary, setSummary] = useState<PaymentSummary>({ period_balance: 0, period_incomes: 0, period_outcomes: 0 })
	const [editTransaction, setEditTransaction] = useState<Transaction | null>(null)
	const [isOpenTransactionModal, setIsOpenTransactionModal] = useState(false)
	const [period, setPeriod] = useState<Period>({ year: new Date().getFullYear(), month: (new Date().getMonth() + 1) })

	function convertedPeriod(period: Period) {
		return `${period.year}-${String(period.month).padStart(2, '0')}`
	}

	async function getTransactions() {
		await api.get(`transaction/get/${convertedPeriod(period)}`, token || {}).then(response => {
			const sortedTransactions = response.data?.transactions.sort((a: Transaction, b: Transaction) => (
				new Date(b.date).getTime() - new Date(a.date).getTime()
			))
			setTransactions(sortedTransactions)
			getAccounts()
		}).catch((error) => {
			setTransactions([])
		})
	}

	async function getSummary() {
		await api.get(`payment/get/summary/${convertedPeriod(period)}`, token || {}).then(response => {
			setSummary(response.data?.payment_summary)
		}).catch((error) => {
			setSummary({ period_balance: 0, period_incomes: 0, period_outcomes: 0 })
		})
	}

	function openTransactionModal(transaction?: Transaction) {
		setEditTransaction(transaction || null)
		setIsOpenTransactionModal(true)
	}

	function closeTransactionModal() {
		setEditTransaction(null)
		getTransactions()
		getSummary()
		setIsOpenTransactionModal(false)
	}

	async function deleteTransaction(TransactionId?: number) {
		const id = TransactionId ? TransactionId : editTransaction?.id
		await api.delete(`transaction/delete/${id}`, token || {}).catch((error) => {
			console.log(error)
		})
		closeTransactionModal()
	}

	useEffect(() => {
		if (token) {
			getTransactions();
			getSummary();
		}
	}, [token, period])

	return (
		<TransactionContext.Provider value={{
			transactions,
			getTransactions,
			openTransactionModal,
			closeTransactionModal,
			isOpenTransactionModal,
			editTransaction,
			deleteTransaction,
			period,
			setPeriod,
			summary
		}}>
			{props.children}
		</TransactionContext.Provider>
	)
}

export const useTransaction = () => useContext(TransactionContext);