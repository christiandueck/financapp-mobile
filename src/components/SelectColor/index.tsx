import React from 'react';
import { ScrollView, StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { Color, useColor } from '../../hooks/useColor';

interface SelectColorProps {
	activeColor: Color;
	setActiveColor: (color: Color) => void;
}


export const SelectColor = ({ activeColor, setActiveColor }: SelectColorProps) => {
	const { colors } = useColor()

	return (
		<View>
			<Text style={styles.label}>Selecione uma cor:</Text>
			<ScrollView horizontal style={styles.container} showsHorizontalScrollIndicator={false}>
				{colors?.map(color => (
					<TouchableOpacity
						key={color.id}
						style={[styles.colorContainer, color.id === activeColor.id && styles.active]}
						onPress={() => setActiveColor(color)}
					>
						<View style={[styles.color, { backgroundColor: color.hex_code }]} />
					</TouchableOpacity>
				))}
			</ScrollView>
		</View>
	)
}

const styles = StyleSheet.create({
	label: {
		color: "#A1A5A5",
		fontSize: 14,
		textTransform: "uppercase",
		marginLeft: 10,
		marginBottom: 6,
	},
	container: {

	},
	colorContainer: {
		padding: 5,
		marginRight: 4,
		borderWidth: 3,
		borderRadius: 20,
		borderColor: 'transparent',
	},
	active: {
		borderColor: '#A1A5A5'
	},
	color: {
		height: 24,
		width: 24,
		borderRadius: 12,
	}
})