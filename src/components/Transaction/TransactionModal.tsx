import React, { useEffect, useState } from 'react';
import { Modal, StyleSheet, Text, View, TouchableOpacity, ScrollView, Platform } from 'react-native';
import { useTransaction } from '../../hooks/useTransaction';
import { Button } from '../Button';
import { MaterialCommunityIcons, Ionicons } from '@expo/vector-icons';
import { FormInput } from '../Form/FormInput';
import { api } from '../../services/api';
import { useUser } from '../../hooks/useUser';
import { SelectTransactionType } from '../SelectTransactionType';
import { Account, useAccount } from '../../hooks/useAccount';
import { Category, useCategory } from '../../hooks/useCategory';
import { SelectAccount } from './SelectAccount';
import { SelectCategory } from './SelectCategory';
import { format } from 'date-fns';
import { SelectDate } from '../Form/SelectDate';

export const TransactionModal = () => {
	const { token } = useUser()
	const { isOpenTransactionModal, closeTransactionModal, editTransaction, deleteTransaction } = useTransaction()
	const { activeAccounts } = useAccount()
	const { activeCategories } = useCategory()

	const [type, setType] = useState<'income' | 'outcome'>('income')
	const [amount, setAmount] = useState<string>('')
	const [date, setDate] = useState<Date>(new Date())
	const [category, setCategory] = useState<Category>(activeCategories.filter(category => category.type === type)[0])
	const [account, setAccount] = useState<Account>(activeAccounts[0])
	const [description, setDescription] = useState<string>('')

	const handleSaveTransaction = async () => {
		if (amount !== '') {
			const transaction = {
				type: type,
				amount: Number(amount.replace(",", ".")),
				date: format(date, 'yyyy-MM-dd'),
				category_id: category.id,
				origin_account_id: account.id,
				description: description !== '' ? description : category.name,
				installments: 1,
			}

			if (editTransaction !== null) {
				await api.post(`transaction/update/${editTransaction.id}`, {
					...transaction,
					id: editTransaction.id
				}, token || {}).catch((error) => {
					console.log(error.response)
				})
			} else {
				await api.post('transaction/insert', transaction, token || {}).then(response => console.log(response)).catch((error) => {
					console.log(error.response)
				})
			}

			closeTransactionModal()
		}
	}

	useEffect(() => {
		setCategory(activeCategories.filter(category => category.type === type)[0])
	}, [type])

	useEffect(() => {
		if (editTransaction !== null) {
			setType(editTransaction.type)
			setAmount(editTransaction.amount.toString())
			setDate(new Date(editTransaction.date.substring(0, 10)))
			setCategory(editTransaction.category)
			setAccount(editTransaction.account)
			setDescription(editTransaction.description)
		} else {
			setType('income')
			setAmount('')
			setDate(new Date())
			setCategory(activeCategories.filter(category => category.type === type)[0])
			setAccount(activeAccounts[0])
			setDescription('')
		}
	}, [isOpenTransactionModal])

	return (
		<Modal
			animationType='fade'
			transparent={true}
			visible={isOpenTransactionModal}
		>
			<View style={styles.overlay}>
				<View style={styles.container}>
					<ScrollView>
						<View style={styles.header}>
							<View style={styles.titleContainer}>
								<MaterialCommunityIcons name="bank-transfer" size={22} color="white" style={{ marginRight: 8 }} />
								<Text style={styles.title}>{editTransaction ? 'Editar transação' : 'Nova transação'}</Text>
							</View>

							<TouchableOpacity onPress={closeTransactionModal}>
								<Ionicons
									name="md-close"
									size={32}
									color="#C7C9C9"
								/>
							</TouchableOpacity>
						</View>

						<View style={{ marginBottom: 30 }}>
							<SelectTransactionType type={type} setType={setType} label="Tipo de transação" />

							<View style={{ height: 14 }} />

							<FormInput
								label="valor"
								keyboardType='decimal-pad'
								placeholder='R$ 0,00'
								value={amount}
								onChangeText={(t) => setAmount(t)}
							/>

							<View style={{ height: 14 }} />

							<SelectDate
								date={date}
								setDate={setDate}
								label={`data da ${type === 'income' ? 'entrada' : 'saída'}`}
							/>

							<View style={{ height: 14 }} />

							<SelectAccount
								accounts={activeAccounts}
								account={account}
								setAccount={setAccount}
								label={`conta de ${type === 'income' ? 'destino' : 'origem'}`}
							/>

							<View style={{ height: 14 }} />

							<SelectCategory
								categories={activeCategories.filter(category => category.type === type)}
								category={category}
								setCategory={setCategory}
								label="Categoria"
							/>

							<View style={{ height: 14 }} />

							<FormInput
								label="Descrição"
								keyboardType='default'
								placeholder='Texto opcional'
								value={description}
								onChangeText={(t) => setDescription(t)}
							/>

							<View style={{ height: 14 }} />
						</View>

						<Button title="Salvar" color="#20B74A" onPress={handleSaveTransaction} />

						{editTransaction !== null &&
							<>
								<View style={{ height: 20 }} />
								<Button title="Excluir" color='#FA4F4F' onPress={() => deleteTransaction()} />
							</>
						}
					</ScrollView>
				</View>
			</View>
		</Modal>
	)
}

const styles = StyleSheet.create({
	overlay: {
		backgroundColor: 'rgba(0, 0, 0, 0.5)',
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	container: {
		width: '100%',
		padding: 30,
		backgroundColor: '#2E3838',
		borderRadius: 16,
	},
	header: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		marginBottom: 30,
	},
	titleContainer: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	title: {
		color: 'white',
		fontSize: 20,
		textTransform: 'uppercase',
		fontWeight: '700',
	}
})