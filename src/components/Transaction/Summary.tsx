import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useTransaction } from '../../hooks/useTransaction';
import { MaterialCommunityIcons } from '@expo/vector-icons';

interface SummaryProps {
	filter: null | 'income' | 'outcome';
	setFilter: (filter: null | 'income' | 'outcome') => void;
}

export const Summary = ({ filter, setFilter }: SummaryProps) => {
	const { summary } = useTransaction();
	const [isExpanded, setIsExpanded] = useState(false)

	return (
		<>
			<View style={isExpanded ? styles.containerExp : styles.container}>
				<TouchableOpacity
					style={[isExpanded ? styles.cardExp : styles.card, { backgroundColor: "rgba(255, 255, 255, 0.3)" }]}
					onPress={() => setFilter(null)}
				>
					<MaterialCommunityIcons
						name="currency-usd-circle-outline"
						size={isExpanded ? 120 : 80}
						color="rgba(255, 255, 255, 0.2)"
						style={isExpanded ? styles.iconExp : styles.icon}
					/>
					<Text style={isExpanded ? styles.labelExp : styles.label}>Saldo</Text>
					<Text style={isExpanded ? styles.amountExp : styles.amount}>R$ {(summary.period_balance || 0).toFixed(2).toString().replace('.', ',')}</Text>
				</TouchableOpacity>

				<View style={isExpanded ? { height: 10 } : { width: 10 }} />

				<TouchableOpacity
					style={[isExpanded ? styles.cardExp : styles.card, { backgroundColor: "rgba(32, 183, 74, 0.1)" }]}
					onPress={() => {
						filter === 'income' ? setFilter(null) : setFilter('income')
					}}
				>
					<MaterialCommunityIcons
						name="arrow-up-bold"
						size={isExpanded ? 120 : 80}
						color="rgba(255, 255, 255, 0.2)"
						style={isExpanded ? styles.iconExp : styles.icon}
					/>
					<Text style={isExpanded ? styles.labelExp : styles.label}>Entradas</Text>
					<Text style={isExpanded ? styles.amountExp : styles.amount}>R$ {(summary.period_incomes || 0).toFixed(2).toString().replace('.', ',')}</Text>
				</TouchableOpacity>

				<View style={isExpanded ? { height: 10 } : { width: 10 }} />

				<TouchableOpacity
					style={[isExpanded ? styles.cardExp : styles.card, { backgroundColor: "rgba(250, 79, 79, 0.2)" }]}
					onPress={() => {
						filter === 'outcome' ? setFilter(null) : setFilter('outcome')
					}}
				>
					<MaterialCommunityIcons
						name="arrow-down-bold"
						size={isExpanded ? 120 : 80}
						color="rgba(255, 255, 255, 0.2)"
						style={isExpanded ? styles.iconExp : styles.icon}
					/>
					<Text style={isExpanded ? styles.labelExp : styles.label}>Saídas</Text>
					<Text style={isExpanded ? styles.amountExp : styles.amount}>R$ {(summary.period_outcomes || 0).toFixed(2).toString().replace('.', ',')}</Text>
				</TouchableOpacity>
			</View>

			<TouchableOpacity
				onPress={() => setIsExpanded(!isExpanded)}
				style={{ width: '100%', paddingVertical: 6, alignItems: 'center' }}
			>
				<MaterialCommunityIcons
					name={isExpanded ? "chevron-up" : "chevron-down"}
					size={32}
					color="rgba(255, 255, 255, 0.2)"
				/>
			</TouchableOpacity>
		</>
	)
}

const styles = StyleSheet.create({
	containerExp: {
		flexDirection: 'column',
		width: '100%',
	},
	container: {
		flexDirection: 'row',
		width: '100%'
	},
	cardExp: {
		position: 'relative',
		alignItems: 'flex-end',
		paddingVertical: 12,
		paddingHorizontal: 24,
		overflow: 'hidden',
		borderRadius: 8,
	},
	card: {
		flex: 1,
		position: 'relative',
		alignItems: 'flex-end',
		paddingVertical: 6,
		paddingHorizontal: 10,
		overflow: 'hidden',
		borderRadius: 8,
	},
	iconExp: {
		position: 'absolute',
		left: -30,
		top: -30,
		transform: [
			{ rotateX: "15deg" },
			{ rotateZ: "15deg" }
		]
	},
	icon: {
		position: 'absolute',
		left: -24,
		top: -18,
		transform: [
			{ rotateX: "15deg" },
			{ rotateZ: "15deg" }
		]
	},
	labelExp: {
		color: 'white',
		textTransform: 'uppercase',
		fontWeight: '500',
		fontSize: 14,
	},
	label: {
		color: 'white',
		textTransform: 'uppercase',
		fontWeight: '500',
		fontSize: 12,
	},
	amountExp: {
		color: 'white',
		fontSize: 20,
		fontWeight: '700',
	},
	amount: {
		color: 'white',
		fontSize: 16,
		fontWeight: '700',
	}
})