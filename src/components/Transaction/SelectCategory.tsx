import React, { useState } from 'react';
import { FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Account } from '../../hooks/useAccount';
import { Badge } from '../Badge';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Category } from '../../hooks/useCategory';

interface SelectCategoryProps {
	label?: string;
	categories: Category[];
	category: Category;
	setCategory: (category: Category) => void;
}

export const SelectCategory = ({ label, categories, category, setCategory }: SelectCategoryProps) => {
	const [open, setOpen] = useState(false)

	return (
		<>
			{label && <Text style={styles.label}>{label}:</Text>}
			<View style={styles.container}>
				<TouchableOpacity style={styles.header} onPress={() => setOpen(!open)}>
					<Badge color={category?.color?.hex_code} title={category.name} />

					<MaterialCommunityIcons name={`chevron-${open ? 'up' : 'down'}`} size={28} color="#DDDFDF" />
				</TouchableOpacity>


				{open &&
					<View style={styles.list}>
						{categories.map((category) => (
							<View key={category.id} style={{ marginBottom: 8, marginRight: 8, }}>
								<TouchableOpacity onPress={() => {
									setCategory(category)
									setOpen(false)
								}}>
									<Badge color={category?.color.hex_code} title={category?.name} account />
								</TouchableOpacity>
							</View>
						))}
					</View>
				}
			</View>
		</>
	)
}

const styles = StyleSheet.create({
	label: {
		color: "#A1A5A5",
		fontSize: 14,
		textTransform: "uppercase",
		marginLeft: 10,
		marginBottom: 6,
	},
	container: {
		backgroundColor: '#565E5E',
		borderRadius: 8,
		overflow: 'hidden'
	},
	header: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingHorizontal: 16,
		paddingVertical: 8,
	},
	list: {
		borderTopWidth: 1,
		borderColor: '#697070',
		paddingHorizontal: 16,
		paddingVertical: 8,
		flexDirection: 'row',
		flexWrap: 'wrap',
	}
})