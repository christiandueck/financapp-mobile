import React from 'react';
import { Text, View, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import { useTransaction } from '../../hooks/useTransaction';
import { format } from 'date-fns';

interface TransactionListProps {
	filter: null | 'income' | 'outcome';
}

export const TransactionList = ({ filter }: TransactionListProps) => {
	const { transactions, openTransactionModal, deleteTransaction } = useTransaction()

	return (
		<>
			<View style={styles.header}>
				<Text style={styles.headerText}>Descrição</Text>
				<Text style={styles.headerText}>Valor (R$)</Text>
			</View>

			<FlatList
				style={{ marginBottom: 20 }}
				data={filter ? transactions.filter((transaction) => transaction.type === filter) : transactions}
				keyExtractor={item => (item.id.toString())}
				renderItem={({ item }) => (
					<View style={styles.card}>
						<View style={styles.content}>
							<Text style={styles.description}>{item.description}</Text>
							<View style={styles.categoryAccount}>
								<Text style={styles.info}>{item.category.name}  | </Text>
								<MaterialIcons name="account-balance" size={12} color="white" style={{ marginHorizontal: 4 }} />
								<Text style={styles.info}>{item.account.name}</Text>
							</View>
							<Text style={styles.info}>{format(new Date(item.date.substring(0, 10)), 'dd/MM/yyyy')}</Text>
						</View>

						<View style={styles.contentRight}>
							<View style={{ flexDirection: 'row', alignItems: 'center', marginRight: 6 }}>
								<Text style={[styles.amount, { color: item.type === 'income' ? '#20B74A' : '#FA4F4F' }]}>
									{item.amount.toFixed(2).toString().replace('.', ',')}
								</Text>
								{item.type === 'income'
									? <MaterialCommunityIcons name="arrow-up-bold" size={20} color="#20B74A" style={{ marginLeft: -6 }} />
									: <MaterialCommunityIcons name="arrow-down-bold" size={20} color="#FA4F4F" style={{ marginLeft: -6 }} />
								}
							</View>

							<View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
								<TouchableOpacity style={styles.editButton} onPress={() => openTransactionModal(item)}>
									<MaterialIcons name="edit" size={24} color="white" />
								</TouchableOpacity>

								<TouchableOpacity style={styles.editButton} onPress={() => deleteTransaction(item.id)}>
									<MaterialIcons name="delete" size={24} color="white" />
								</TouchableOpacity>
							</View>
						</View>
					</View>
				)}
			/>
		</>
	)
}

const styles = StyleSheet.create({
	header: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		borderBottomWidth: 2,
		borderColor: '#20B74A',
	},
	headerText: {
		color: 'white',
		textTransform: 'uppercase',
		fontWeight: '700',
		paddingVertical: 6,
		paddingHorizontal: 12,
		fontSize: 18,
	},
	card: {
		flexDirection: 'row',
		alignItems: 'center',
		paddingVertical: 6,
		paddingLeft: 6,
		borderBottomWidth: 1,
		borderColor: '#565E5E',
		flex: 1,
	},
	content: {
		padding: 4,
		flex: 1,
	},
	color: {
		width: 24,
		height: 24,
		borderRadius: 12,
	},
	description: {
		textTransform: 'capitalize',
		fontSize: 18,
		color: 'white',
	},
	categoryAccount: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	info: {
		textTransform: 'capitalize',
		fontSize: 16,
		color: '#C7C9C9',
	},
	contentRight: {
		flexDirection: 'column'
	},
	amount: {
		fontSize: 20,
		marginRight: 10,
	},
	editButton: {
		padding: 8,
		alignItems: 'center',
		justifyContent: 'center',
	}
})