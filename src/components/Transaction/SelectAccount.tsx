import React, { useState } from 'react';
import { FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Account } from '../../hooks/useAccount';
import { Badge } from '../Badge';
import { MaterialCommunityIcons } from '@expo/vector-icons';

interface SelectAccountProps {
	label?: string;
	accounts: Account[];
	account: Account;
	setAccount: (account: Account) => void;
}

export const SelectAccount = ({ label, accounts, account, setAccount }: SelectAccountProps) => {
	const [open, setOpen] = useState(false)

	return (
		<>
			{label && <Text style={styles.label}>{label}:</Text>}
			<View style={styles.container}>
				<TouchableOpacity style={styles.header} onPress={() => setOpen(!open)}>
					<Badge color={account?.color?.hex_code} title={account.name} account />

					<MaterialCommunityIcons name={`chevron-${open ? 'up' : 'down'}`} size={28} color="#DDDFDF" />
				</TouchableOpacity>

				{open &&
					<View style={styles.list}>
						{accounts.map((account) => (
							<View key={account.id} style={{ marginBottom: 8, marginRight: 8, }}>
								<TouchableOpacity onPress={() => {
									setAccount(account)
									setOpen(false)
								}}>
									<Badge color={account?.color.hex_code} title={account?.name} account />
								</TouchableOpacity>
							</View>
						))}
					</View>
				}
			</View>
		</>
	)
}

const styles = StyleSheet.create({
	label: {
		color: "#A1A5A5",
		fontSize: 14,
		textTransform: "uppercase",
		marginLeft: 10,
		marginBottom: 6,
	},
	container: {
		backgroundColor: '#565E5E',
		borderRadius: 8,
		overflow: 'hidden'
	},
	header: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingHorizontal: 16,
		paddingVertical: 8,
	},
	list: {
		borderTopWidth: 1,
		borderColor: '#697070',
		paddingHorizontal: 16,
		paddingVertical: 8,
		flexDirection: 'row',
		flexWrap: 'wrap',
	}
})