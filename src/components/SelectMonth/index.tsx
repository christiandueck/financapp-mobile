import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useTransaction } from '../../hooks/useTransaction';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { format } from 'date-fns';
import { ptBR } from 'date-fns/locale';

export const SelectMonth = () => {
	const { period, setPeriod } = useTransaction()

	function changeMonth(aggregator: 1 | -1) {
		if (aggregator > 0) {
			if (period.month === 12) {
				setPeriod({ year: period.year + 1, month: 1 })
			} else {
				setPeriod({ ...period, month: period.month + 1 })
			}
		} else {
			if (period.month === 1) {
				setPeriod({ year: period.year - 1, month: 12 })
			} else {
				setPeriod({ ...period, month: period.month - 1 })
			}
		}
	}

	return (
		<View style={styles.container}>
			<TouchableOpacity onPress={() => changeMonth(-1)}>
				<MaterialCommunityIcons name="chevron-left" size={40} color="#DDDFDF" />
			</TouchableOpacity>

			<Text style={styles.text}>{format(new Date(period.year, period.month - 1), 'MMMM yyyy', { locale: ptBR })}</Text>

			<TouchableOpacity onPress={() => changeMonth(1)}>
				<MaterialCommunityIcons name="chevron-right" size={40} color="#DDDFDF" />
			</TouchableOpacity>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		width: '100%',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center'
	},
	text: {
		fontWeight: '700',
		fontSize: 20,
		color: '#DDDFDF',
		textTransform: 'uppercase',
		marginHorizontal: 20,
	}
})