import React from 'react';
import { Text, View, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import { useCategory } from '../../hooks/useCategory';
import { MaterialIcons } from '@expo/vector-icons';

interface CategoryListProps {
	active: boolean;
	type: 'income' | 'outcome'
}

export const CategoryList = ({ active, type }: CategoryListProps) => {
	const { categories, openCategoryModal, deactivateCategory } = useCategory()

	return (
		<>
			<View style={styles.header}>
				<Text style={styles.headerText}>Nome</Text>
				<Text style={styles.headerText}>Ações</Text>
			</View>

			<FlatList
				data={categories.filter(category => category.active === active && category.type === type)}
				keyExtractor={item => (item.id.toString())}
				renderItem={({ item }) => (
					<View style={styles.card}>
						<View style={[styles.color, { backgroundColor: item.color.hex_code }]} />

						<View style={styles.content}>
							<Text style={styles.name}>{item.name}</Text>
						</View>

						<TouchableOpacity style={styles.editButton} onPress={() => openCategoryModal(item)}>
							<MaterialIcons name="edit" size={24} color="white" />
						</TouchableOpacity>

						{item.active &&
							<TouchableOpacity style={styles.editButton} onPress={() => deactivateCategory(item.id)}>
								<MaterialIcons name="delete" size={24} color="white" />
							</TouchableOpacity>
						}
					</View>
				)}
			/>
		</>
	)
}

const styles = StyleSheet.create({
	header: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		borderBottomWidth: 2,
		borderColor: '#20B74A',
	},
	headerText: {
		color: 'white',
		textTransform: 'uppercase',
		fontWeight: '700',
		paddingVertical: 6,
		paddingHorizontal: 12,
		fontSize: 18,
	},
	card: {
		flexDirection: 'row',
		alignItems: 'center',
		paddingVertical: 6,
		paddingLeft: 6,
		borderBottomWidth: 1,
		borderColor: '#565E5E',
		flex: 1,
	},
	content: {
		padding: 12,
		flex: 1,
	},
	color: {
		width: 24,
		height: 24,
		borderRadius: 12,
	},
	name: {
		textTransform: 'capitalize',
		fontSize: 18,
		color: 'white',
	},
	editButton: {
		height: '100%',
		aspectRatio: 1,
		alignItems: 'center',
		justifyContent: 'center',
	}
})