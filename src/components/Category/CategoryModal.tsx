import React, { useEffect, useState } from 'react';
import { Modal, StyleSheet, Text, View, TouchableOpacity, ScrollView } from 'react-native';
import { useCategory } from '../../hooks/useCategory';
import { Button } from '../Button';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';
import { FormInput } from '../Form/FormInput';
import { Color, useColor } from '../../hooks/useColor';
import { api } from '../../services/api';
import { SelectColor } from '../SelectColor';
import { useUser } from '../../hooks/useUser';
import { SelectTransactionType } from '../SelectTransactionType';

export const CategoryModal = () => {
	const { token } = useUser()
	const { isOpenCategoryModal, closeCategoryModal, editCategory, deactivateCategory } = useCategory()
	const { colors } = useColor()

	const [type, setType] = useState<'income' | 'outcome'>('income')
	const [name, setName] = useState<string>('')
	const [color, setColor] = useState<Color>(colors[0])

	const handleSaveCategory = async () => {
		if (name !== '') {
			const category = {
				type: type,
				name: name,
				color_id: color.id
			}

			if (editCategory !== null) {
				await api.post(`category/update/${editCategory.id}`, {
					...category,
					id: editCategory.id,
					active: editCategory.active,
				}, token || {}).catch((error) => {
					console.log(error.response)
				})
			} else {
				await api.post('category/insert', category, token || {}).catch((error) => {
					console.log(error)
				})
			}

			closeCategoryModal()
		}
	}

	async function reactivate() {
		await api.post(`category/update/${editCategory?.id}`, {
			id: editCategory?.id,
			name: name,
			type: type,
			color_id: color.id,
			active: true,
		}, token || {})

		closeCategoryModal()
	}

	useEffect(() => {
		if (editCategory !== null) {
			setType(editCategory.type)
			setName(editCategory.name)
			setColor(editCategory.color)
		} else {
			setType('income')
			setName('')
			setColor(colors[0])
		}
	}, [isOpenCategoryModal])

	return (
		<Modal
			animationType='fade'
			transparent={true}
			visible={isOpenCategoryModal}
		>
			<View style={styles.overlay}>
				<View style={styles.container}>
					<View style={styles.header}>
						<View style={styles.titleContainer}>
							<MaterialIcons name="category" size={22} color="white" style={{ marginRight: 8 }} />
							<Text style={styles.title}>{editCategory ? 'Editar categoria' : 'Nova categoria'}</Text>
						</View>

						<TouchableOpacity onPress={closeCategoryModal}>
							<Ionicons
								name="md-close"
								size={32}
								color="#C7C9C9"
							/>
						</TouchableOpacity>
					</View>

					<ScrollView style={{ marginBottom: 30 }}>
						<SelectTransactionType type={type} setType={setType} label="Tipo de categoria" />

						<View style={{ height: 14 }} />

						<FormInput
							label="nome"
							keyboardType='default'
							value={name}
							onChangeText={(t) => setName(t)}
						/>

						<View style={{ height: 14 }} />

						<SelectColor activeColor={color} setActiveColor={setColor} />
					</ScrollView>

					<Button title="Salvar" color="#20B74A" onPress={handleSaveCategory} />

					{editCategory !== null &&
						<>
							<View style={{ height: 20 }} />
							{editCategory.active
								? <Button title="Desativar categoria" color='#FA4F4F' onPress={() => deactivateCategory()} />
								: <Button title="Reativar categoria" color='#F08F49' onPress={reactivate} />
							}
						</>
					}
				</View>
			</View>
		</Modal>
	)
}

const styles = StyleSheet.create({
	overlay: {
		backgroundColor: 'rgba(0, 0, 0, 0.5)',
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	container: {
		width: '100%',
		padding: 30,
		backgroundColor: '#2E3838',
		borderRadius: 16,
	},
	header: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		marginBottom: 30,
	},
	titleContainer: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	title: {
		color: 'white',
		fontSize: 20,
		textTransform: 'uppercase',
		fontWeight: '700',
	}
})