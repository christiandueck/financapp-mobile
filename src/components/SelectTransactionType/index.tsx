import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from "react-native"
import { MaterialCommunityIcons } from '@expo/vector-icons';

interface SelectTransactionTypeProps {
	type: 'income' | 'outcome',
	setType: (type: 'income' | 'outcome') => void;
	label?: string;
}

export const SelectTransactionType = ({ label, type, setType }: SelectTransactionTypeProps) => {
	return (
		<>
			{label && <Text style={styles.label}>{label}:</Text>}

			<View style={styles.selectType}>
				<TouchableOpacity
					style={[styles.typeButton, type === 'income' ? styles.activeButton : {}]}
					onPress={() => setType('income')}
				>
					<MaterialCommunityIcons name="arrow-up-bold" size={28} color="#20B74A" style={{ marginRight: 2 }} />
					<Text style={[styles.buttonText, type === 'income' ? styles.activeButtonText : {}]}>Entrada</Text>
				</TouchableOpacity>

				<View style={{ width: 16 }} />

				<TouchableOpacity
					style={[styles.typeButton, type === 'outcome' ? styles.activeButton : {}]}
					onPress={() => setType('outcome')}
				>
					<MaterialCommunityIcons name="arrow-down-bold" size={28} color="#FA4F4F" style={{ marginRight: 2 }} />
					<Text style={[styles.buttonText, type === 'outcome' ? styles.activeButtonText : {}]}>Saída</Text>
				</TouchableOpacity>
			</View>
		</>
	)
}

const styles = StyleSheet.create({
	label: {
		color: "#A1A5A5",
		fontSize: 14,
		textTransform: "uppercase",
		marginLeft: 10,
		marginBottom: 6,
	},
	selectType: {
		flexDirection: 'row',
	},
	typeButton: {
		paddingVertical: 8,
		paddingHorizontal: 16,
		borderWidth: 3,
		borderRadius: 8,
		borderColor: '#697070',
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		flex: 1,
	},
	button: {
		paddingVertical: 8,
		paddingHorizontal: 16,
		borderWidth: 3,
		borderRadius: 8,
		borderColor: '#697070',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		marginBottom: 20,
	},
	buttonText: {
		color: 'white',
		fontSize: 16,
		textTransform: 'uppercase',
	},
	activeButton: {
		backgroundColor: '#202727',
		borderColor: '#202727',
	},
	activeButtonText: {
		fontWeight: '700'
	}
})