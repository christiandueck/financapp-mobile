import { DrawerContentComponentProps, DrawerContentScrollView } from '@react-navigation/drawer';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Ionicons, MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import { DrawerActions } from '@react-navigation/native';

const DrawerMenu = (props: DrawerContentComponentProps) => {
	return (
		<DrawerContentScrollView {...props}>
			<View style={styles.drawerContainer}>
				<View style={styles.drawerHeader}>
					<Ionicons name="md-close" size={32} color="#C7C9C9"
						onPress={() => props.navigation.dispatch(DrawerActions.closeDrawer())} />
					<Text style={styles.drawerTitle}>Menu</Text>
					<View style={{ width: 32 }} />
				</View>

				<TouchableOpacity style={styles.link}
					onPress={() => props.navigation.navigate("Transactions")}
				>
					<MaterialCommunityIcons name="bank-transfer" size={22} color={props.state.index === 0 ? "#13B7B7" : "#DDDFDF"} />
					<Text style={props.state.index === 0 ? styles.activeLink : styles.linkText}>Transações</Text>
				</TouchableOpacity>

				<TouchableOpacity style={styles.link}
					onPress={() => props.navigation.navigate("Accounts")}
				>
					<MaterialIcons name="account-balance" size={22} color={props.state.index === 1 ? "#13B7B7" : "#DDDFDF"} />
					<Text style={props.state.index === 1 ? styles.activeLink : styles.linkText}>Contas</Text>
				</TouchableOpacity>

				<TouchableOpacity style={styles.link}
					onPress={() => props.navigation.navigate("Categories")}
				>
					<MaterialIcons name="category" size={22} color={props.state.index === 2 ? "#13B7B7" : "#DDDFDF"} />
					<Text style={props.state.index === 2 ? styles.activeLink : styles.linkText}>Categorias</Text>
				</TouchableOpacity>
			</View>
		</DrawerContentScrollView>
	)
}

const styles = StyleSheet.create({
	drawerContainer: {
		padding: 24,
	},
	drawerHeader: {
		marginBottom: 24,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
	},
	drawerTitle: {
		fontSize: 20,
		fontWeight: "700",
		textTransform: "uppercase",
		color: "#DDDFDF",
	},
	link: {
		paddingHorizontal: 12,
		paddingVertical: 18,
		display: "flex",
		flexDirection: "row",
		alignItems: "center",
		borderBottomWidth: 1,
		borderBottomColor: "#434C4C"
	},
	linkText: {
		marginLeft: 12,
		color: "#DDDFDF",
		fontSize: 18,
		fontWeight: "700",
		textTransform: "uppercase",
	},
	activeLink: {
		marginLeft: 12,
		fontSize: 18,
		fontWeight: "700",
		textTransform: "uppercase",
		color: "#13B7B7",
		textDecorationLine: 'underline',
	}
})

export default DrawerMenu;