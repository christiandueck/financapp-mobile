import React from 'react';
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { DrawerActions } from '@react-navigation/routers';
import { useNavigation } from '@react-navigation/core';
import Avatar from './Avatar';

const Header = () => {
	const navigation = useNavigation()

	return (
		<View style={styles.headerContainer}>
			<TouchableOpacity
				onPress={() => navigation.dispatch(DrawerActions.openDrawer())}
			>
				<Ionicons name="md-menu" size={40} color="#C7C9C9" />
			</TouchableOpacity>

			<Image style={styles.logo} source={require('../../../assets/logo.png')} />

			<Avatar />
		</View>
	)
}

const generateColor = () => {
	const randomColor = Math.floor(Math.random() * 16777215)
		.toString(16)
		.padStart(6, '0');
	return `#${randomColor}`;
};

const styles = StyleSheet.create({
	headerContainer: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		paddingVertical: 16,
		paddingHorizontal: 20,
		zIndex: 10,
	},
	logo: {
		width: 169,
		height: 32,
	},
})

export default Header;
