import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import { useUser } from '../../hooks/useUser';
import UserMenu from './UserMenu';

const Avatar = () => {
	const navigation = useNavigation()
	const { user } = useUser();

	const [isUserMenuOpen, setIsUserMenuOpen] = useState(false)

	function toggleUserMenu() {
		setIsUserMenuOpen(!isUserMenuOpen)
	}

	const names = user ? user?.name.split(" ") : "No User"
	let initials = names[0].substring(0, 1)
	if (names.length > 1) {
		initials += names[names.length - 1].substring(0, 1)
	}

	return (
		<>
			<TouchableOpacity style={styles.avatarButton} onPress={toggleUserMenu}>
				<View style={styles.avatarContainer}>
					<Text style={styles.initials}>{initials}</Text>
				</View>
			</TouchableOpacity>

			{isUserMenuOpen &&
				<UserMenu closeFunction={toggleUserMenu} />
			}
		</>
	)
}

const generateColor = () => {
	const randomColor = Math.floor(Math.random() * 16777215)
		.toString(16)
		.padStart(6, '0');
	return `#${randomColor}`;
};

const styles = StyleSheet.create({
	avatarButton: {
		position: 'relative',
	},
	avatarContainer: {
		width: 44,
		height: 44,
		backgroundColor: generateColor(),
		borderRadius: 14,
		borderWidth: 4,
		borderColor: 'rgba(255, 255, 255, 0.6)',
		justifyContent: "center",
		alignItems: "center",
	},
	initials: {
		fontSize: 18,
		fontWeight: "700",
		color: "white",
		textTransform: "uppercase",
	}
})

export default Avatar;
