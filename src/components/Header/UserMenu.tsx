import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useUser } from '../../hooks/useUser';
import { Ionicons } from '@expo/vector-icons';

interface UserMenuProps {
	closeFunction: () => void;
}

const UserMenu = ({ closeFunction }: UserMenuProps) => {
	const { user, signOut } = useUser()

	return (
		<View style={styles.menuContainer}>
			<View style={styles.userMenuHeader}>
				<View style={{ flexDirection: "row" }}>
					<Text style={styles.greeting}>Olá, </Text>
					<Text style={styles.name}>{user?.name.split(" ")[0]}</Text>
				</View>

				<TouchableOpacity
					style={{ marginLeft: 14, marginRight: -6 }}
					onPress={closeFunction}
				>
					<Ionicons
						name="md-close"
						size={32}
						color="#C7C9C9"
					/>
				</TouchableOpacity>
			</View>

			<TouchableOpacity style={styles.link}
				onPress={signOut}
			>
				<Ionicons name="md-exit-outline" size={24} color="#C7C9C9" />
				<Text style={styles.linkText}>Sair</Text>
			</TouchableOpacity>
		</View>
	)
}

const styles = StyleSheet.create({
	menuContainer: {
		position: 'absolute',
		top: 72,
		right: 20,
		backgroundColor: "#434C4C",
		borderRadius: 10,
		overflow: 'hidden',
	},
	userMenuHeader: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		paddingHorizontal: 20,
		paddingVertical: 16,
		borderBottomWidth: 1,
		borderBottomColor: "#20B74A",
	},
	greeting: {
		color: "white",
		textTransform: 'uppercase',
		fontSize: 18,
	},
	name: {
		color: "white",
		fontWeight: "700",
		textTransform: "uppercase",
		fontSize: 18,
	},
	link: {
		padding: 20,
		display: "flex",
		flexDirection: "row",
		alignItems: "center",
	},
	linkText: {
		marginLeft: 12,
		color: "#DDDFDF",
		fontSize: 16,
		textTransform: "uppercase",
	},
})

export default UserMenu;