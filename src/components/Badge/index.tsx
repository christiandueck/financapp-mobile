import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';

interface BadgeProps {
	title: string;
	account?: boolean;
	color: string;
}

export const Badge = ({ title, color, account }: BadgeProps) => {
	return (
		<View style={[styles.container, { backgroundColor: color || '#202727' }]}>
			{account && <MaterialIcons name="account-balance" size={14} color="white" style={{ marginRight: 4 }} />}
			<Text style={styles.title}>{title}</Text>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		paddingHorizontal: 10,
		paddingVertical: 5,
		borderRadius: 4,
		flexDirection: 'row',
		alignItems: 'center',
		alignSelf: 'flex-start',
	},
	title: {
		color: 'white',
		fontWeight: '500',
		fontSize: 16,
	}
})