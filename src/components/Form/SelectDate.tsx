import React, { useState } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import DateTimePicker, { Event } from '@react-native-community/datetimepicker';
import { format } from 'date-fns';

interface SelectDateProps {
	label?: string;
	date: Date;
	setDate: (date: Date) => void;
}

export const SelectDate = ({ label, date, setDate }: SelectDateProps) => {
	const [showDatePicker, setShowDatePicker] = useState(false);

	function handleChangeTime(event: Event, dateTime: Date | undefined) {
		if (Platform.OS === 'android') {
			setShowDatePicker(oldState => !oldState);
		}

		if (dateTime) {
			setDate(dateTime);
		}
	}

	return (
		<View>
			{label && <Text style={styles.label}>{label}:</Text>}
			{showDatePicker && (
				<DateTimePicker
					value={date}
					mode="date"
					display="default"
					onChange={handleChangeTime}
				/>
			)}

			{Platform.OS === 'android' && (
				<TouchableOpacity style={styles.input} onPress={() => setShowDatePicker(oldState => !oldState)}>
					<Text style={styles.text}>{format(date, 'dd/MM/yyyy')}</Text>
				</TouchableOpacity>
			)}
		</View>
	)
}

const styles = StyleSheet.create({
	label: {
		color: "#A1A5A5",
		fontSize: 14,
		textTransform: "uppercase",
		marginLeft: 10,
		marginBottom: 6,
	},
	input: {
		backgroundColor: "#565E5E",
		paddingVertical: 8,
		paddingHorizontal: 14,
		borderRadius: 8,
	},
	text: {
		fontSize: 20,
		color: "white",
	}
})