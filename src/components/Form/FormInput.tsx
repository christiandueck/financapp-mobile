import * as React from 'react';
import { View, Text, StyleSheet, Image, TextInput, TextInputProps } from 'react-native';

interface FormInputProps extends TextInputProps {
	label?: string;
}

export const FormInput = ({ label, ...rest }: FormInputProps) => {

	return (
		<View>
			{label && <Text style={styles.label}>{label}:</Text>}
			<TextInput
				autoCapitalize="none"
				autoCorrect={false}
				style={styles.textInput}
				placeholderTextColor="#DDDFDF"
				{...rest}
			/>
		</View>
	)
}

const styles = StyleSheet.create({
	label: {
		color: "#A1A5A5",
		fontSize: 14,
		textTransform: "uppercase",
		marginLeft: 10,
		marginBottom: 6,
	},
	textInput: {
		fontSize: 20,
		color: "white",
		backgroundColor: "#565E5E",
		paddingVertical: 8,
		paddingHorizontal: 14,
		borderRadius: 8,
	}
});
