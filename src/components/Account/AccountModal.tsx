import React, { useEffect, useState } from 'react';
import { Modal, StyleSheet, Text, View, TouchableOpacity, ScrollView } from 'react-native';
import { useAccount } from '../../hooks/useAccount';
import { Button } from '../Button';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';
import { FormInput } from '../Form/FormInput';
import { Color, useColor } from '../../hooks/useColor';
import { api } from '../../services/api';
import { SelectColor } from '../SelectColor';
import { useUser } from '../../hooks/useUser';

export const AccountModal = () => {
	const { token } = useUser()
	const { isOpenAccountModal, closeAccountModal, editAccount, deactivateAccount } = useAccount()
	const { colors } = useColor()

	const [name, setName] = useState<string>('')
	const [balance, setBalance] = useState<string>('')
	const [color, setColor] = useState<Color>(colors[0])

	const handleSaveAccount = async () => {
		if (name !== '') {
			const account = {
				type: 'bank',
				name: name,
				balance: Number(balance.replace(",", ".")),
				color_id: color.id
			}

			if (editAccount !== null) {
				await api.post(`account/update/${editAccount.id}`, {
					...account,
					id: editAccount.id,
					active: editAccount.active,
				}, token || {}).catch((error) => {
					console.log(error.response)
				})
			} else {
				await api.post('account/insert', account, token || {}).catch((error) => {
					console.log(error)
				})
			}

			closeAccountModal()
		}
	}

	async function reactivate() {
		await api.post(`account/update/${editAccount?.id}`, {
			id: editAccount?.id,
			name: name,
			type: 'bank',
			color_id: color.id,
			balance: Number(balance.replace(",", ".")),
			active: true,
			invoice_closing_date: 1,
			invoice_due_date: 1,
		}, token || {})

		closeAccountModal()
	}

	useEffect(() => {
		if (editAccount !== null) {
			setName(editAccount.name)
			setBalance(editAccount.balance.toString().replace('.', ','))
			setColor(editAccount.color)
		} else {
			setName('')
			setBalance('')
			setColor(colors[0])
		}
	}, [isOpenAccountModal])

	return (
		<Modal
			animationType='fade'
			transparent={true}
			visible={isOpenAccountModal}
		>
			<View style={styles.overlay}>
				<View style={styles.container}>
					<View style={styles.header}>
						<View style={styles.titleContainer}>
							<MaterialIcons name="account-balance" size={22} color="white" style={{ marginRight: 8 }} />
							<Text style={styles.title}>{editAccount ? 'Editar conta' : 'Nova conta'}</Text>
						</View>

						<TouchableOpacity onPress={closeAccountModal}>
							<Ionicons
								name="md-close"
								size={32}
								color="#C7C9C9"
							/>
						</TouchableOpacity>
					</View>
					<ScrollView style={{ marginBottom: 30 }}>
						<FormInput
							label="nome"
							keyboardType='default'
							value={name}
							onChangeText={(t) => setName(t)}
						/>

						<View style={{ height: 14 }} />

						<FormInput
							label="Saldo inicial"
							keyboardType='decimal-pad'
							placeholder='R$ 0,00'
							value={balance}
							onChangeText={(t) => setBalance(t)}
						/>

						<View style={{ height: 14 }} />

						<SelectColor activeColor={color} setActiveColor={setColor} />
					</ScrollView>

					<Button title="Salvar" color="#20B74A" onPress={handleSaveAccount} />

					{editAccount !== null &&
						<>
							<View style={{ height: 20 }} />
							{editAccount.active
								? <Button title="Desativar conta" color='#FA4F4F' onPress={() => deactivateAccount()} />
								: <Button title="Reativar conta" color='#F08F49' onPress={reactivate} />
							}
						</>
					}
				</View>
			</View>
		</Modal>
	)
}

const styles = StyleSheet.create({
	overlay: {
		backgroundColor: 'rgba(0, 0, 0, 0.5)',
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	container: {
		width: '100%',
		padding: 30,
		backgroundColor: '#2E3838',
		borderRadius: 16,
	},
	header: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		marginBottom: 30,
	},
	titleContainer: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	title: {
		color: 'white',
		fontSize: 20,
		textTransform: 'uppercase',
		fontWeight: '700',
	}
})