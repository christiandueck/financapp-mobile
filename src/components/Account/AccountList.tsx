import React from 'react';
import { Text, View, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import { useAccount } from '../../hooks/useAccount';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';

interface AccountListProps {
	active: boolean;
}

export const AccountList = ({ active }: AccountListProps) => {
	const { accounts, openAccountModal } = useAccount()

	return (
		<FlatList
			data={accounts.filter(account => account.active === active)}
			keyExtractor={item => (item.id.toString())}
			renderItem={({ item }) => (
				<View style={styles.card}>
					<View style={[styles.icon, { backgroundColor: item.color.hex_code }]} >
						<MaterialIcons name="account-balance" size={24} color="white" />
					</View>

					<View style={styles.content}>
						<Text style={styles.name}>{item.name}</Text>
						<Text style={styles.label}>Saldo Atual:</Text>
						<Text style={[styles.balance, { color: item.balance >= 0 ? '#20B74A' : '#FA4F4F' }]}>
							R$ {item.balance.toFixed(2).toString().replace('.', ',')}</Text>
					</View>

					<TouchableOpacity style={styles.editButton} onPress={() => openAccountModal(item)}>
						<MaterialIcons name="edit" size={24} color="white" />
					</TouchableOpacity>
				</View>
			)}
			ItemSeparatorComponent={() => (<View style={{ height: 20 }} />)}
		/>
	)
}

const styles = StyleSheet.create({
	card: {
		borderRadius: 8,
		backgroundColor: '#434C4C',
		flexDirection: 'row',
		overflow: 'hidden',
		height: 90,
		alignItems: 'center',
	},
	icon: {
		width: 60,
		height: '100%',
		alignItems: 'center',
		justifyContent: 'center',
	},
	content: {
		padding: 12,
		flex: 1,
	},
	name: {
		textTransform: 'uppercase',
		fontWeight: '700',
		fontSize: 18,
		color: 'white',
		marginBottom: 6,
	},
	label: {
		color: '#C7C9C9',
		textTransform: 'uppercase',
	},
	balance: {
		fontWeight: '600',
		fontSize: 18,
	},
	editButton: {
		height: '90%',
		width: 60,
		alignItems: 'center',
		justifyContent: 'center',
		borderLeftWidth: 1,
		borderLeftColor: '#565E5E',
	}
})