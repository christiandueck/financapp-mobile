import React from 'react';
import { Text, StyleSheet, TouchableOpacityProps, TouchableOpacity } from 'react-native';

interface FormInputProps extends TouchableOpacityProps {
	title: string;
	color?: string;
}

export const Button = ({ title, color = "#20B74A", ...rest }: FormInputProps) => {
	return (
		<TouchableOpacity
			style={[styles.button, { backgroundColor: color }]}
			{...rest}
		>
			<Text style={styles.label}>{title}</Text>
		</TouchableOpacity>
	)
}

const styles = StyleSheet.create({
	button: {
		backgroundColor: "#565E5E",
		padding: 12,
		borderRadius: 8,
		justifyContent: "center",
		alignItems: "center",
	},
	label: {
		fontSize: 18,
		fontWeight: "700",
		color: "white",
	}
});
