import React, { useEffect, useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native';
import { FormInput } from '../../components/Form/FormInput';
import { Button } from '../../components/Button';
import { useUser } from '../../hooks/useUser';
import { api } from '../../services/api';

const UserRegistrationScreen = ({ navigation }: any) => {
	const { user, signIn } = useUser()

	const [name, setName] = useState<string>('')
	const [email, setEmail] = useState<string>('')
	const [password, setPassword] = useState<string>('')

	async function submitForm() {
		if (name !== '' && email !== '' && password !== '') {
			await api.post('register', {
				name, email, password
			})
				.then((response) => {
					if (response.data.status) {
						api.post('login', {
							email,
							password
						})
							.then((response) => {
								if (response.data.status) {
									signIn(response.data.token, response.data.user)
									setName("")
									setEmail("")
									setPassword("")
								}
							})
					}
				})
				.catch((error) => console.log(error.response))
		}
	}

	useEffect(() => {
		if (user) {
			navigation.navigate('Transaction')
		}
	}, [user])

	return (
		<View style={styles.container}>
			<View style={styles.centeredContainer}>
				<Image style={styles.logo} source={require('../../../assets/logo.png')} />
				<View style={styles.formBox}>
					<Text style={styles.title}>
						Novo cadastro
					</Text>

					<FormInput
						label="Nome"
						value={name}
						onChangeText={(t) => setName(t)}
					/>

					<View style={{ height: 14 }} />

					<FormInput
						label="Email"
						placeholder="seu@email.com"
						keyboardType="email-address"
						value={email}
						onChangeText={(t) => setEmail(t)}
					/>

					<View style={{ height: 14 }} />

					<FormInput
						label="senha"
						secureTextEntry={true}
						value={password}
						onChangeText={(t) => setPassword(t)}
					/>

					<View style={{ height: 24 }} />

					<Button title="Cadastrar" onPress={submitForm} />

					<View style={{ height: 24 }} />

					<Button title="Cancelar" color="#FA4F4F" onPress={() => { navigation.navigate("Login") }} />
				</View>
			</View>

			<StatusBar />
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#2E3838',
		alignItems: 'center',
		justifyContent: 'center',
	},
	centeredContainer: {
		width: "100%",
		paddingHorizontal: 30,
		flexDirection: 'column',
	},
	logo: {
		width: 200,
		height: 38,
	},
	formBox: {
		width: "100%",
		marginVertical: 20,
		backgroundColor: "#434C4C",
		padding: 20,
		borderRadius: 16,
	},
	title: {
		color: "white",
		fontSize: 18,
		textTransform: "uppercase",
		fontWeight: "700",
		marginBottom: 24
	}
});

export default UserRegistrationScreen;