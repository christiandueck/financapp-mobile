import React, { useEffect, useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { FormInput } from '../components/Form/FormInput';
import { Button } from '../components/Button';
import { useUser } from '../hooks/useUser';
import { api } from '../services/api';

const LoginScreen = ({ navigation }: any) => {
	const { user, signIn } = useUser()

	const [email, setEmail] = useState<string>('')
	const [password, setPassword] = useState<string>('')
	const [errorMessage, setErrorMessage] = useState<string>('')

	async function submitForm() {
		if (email !== '' && password !== '') {
			await api.post('login', {
				email,
				password
			})
				.then((response) => {
					if (response.data.status) {
						signIn(response.data.token, response.data.user)
						setEmail("")
						setPassword("")
						setErrorMessage("")
					}
				})
				.catch((error) => {
					console.log(error)
					setErrorMessage("Usuário ou senha incorretos!")
					setTimeout(() => setErrorMessage(""), 5000)
				})
		} else {
			setErrorMessage("Todos os campos são obrigatórios!")
			setTimeout(() => setErrorMessage(""), 5000)
		}
	}

	useEffect(() => {
		if (user) {
			navigation.navigate('AuthenticatedScreen')
		}
	}, [user])

	return (
		<View style={styles.container}>
			<View style={styles.centeredContainer}>
				<Image style={styles.logo} source={require('../../assets/logo.png')} />
				<View style={styles.formBox}>
					{errorMessage !== '' &&
						<Text style={styles.error}>{errorMessage}</Text>
					}

					<FormInput
						label="usuário"
						placeholder="seu@email.com"
						keyboardType="email-address"
						value={email}
						onChangeText={(t) => setEmail(t)}
					/>

					<View style={{ height: 14 }} />

					<FormInput
						label="senha"
						secureTextEntry={true}
						value={password}
						onChangeText={(t) => setPassword(t)}
					/>

					<View style={{ height: 24 }} />

					<Button title="Entrar" onPress={submitForm} />
				</View>

				<View style={styles.bottomLinks}>
					<TouchableOpacity style={styles.button} onPress={() => navigation.navigate("UserRegistration")}>
						<Text style={styles.textButton}>Novo Cadastro</Text>
					</TouchableOpacity>
				</View>
			</View>

			<StatusBar />
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#2E3838',
		alignItems: 'center',
		justifyContent: 'center',
	},
	centeredContainer: {
		width: "100%",
		paddingHorizontal: 30,
		flexDirection: 'column',
	},
	logo: {
		width: 200,
		height: 38,
	},
	formBox: {
		width: "100%",
		marginVertical: 20,
		backgroundColor: "#434C4C",
		padding: 20,
		borderRadius: 16,
	},
	bottomLinks: {
		flexDirection: "row",
		justifyContent: "center",
	},
	button: {
		paddingHorizontal: 10,
		paddingVertical: 6,
	},
	textButton: {
		color: "white",
		fontSize: 16,
		fontWeight: "700",
		textTransform: "uppercase"
	},
	error: {
		color: "#FA4F4F",
		fontSize: 16,
		marginBottom: 20,
	}
});

export default LoginScreen;