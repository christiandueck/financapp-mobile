import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { AccountList } from '../components/Account/AccountList';
import { AccountModal } from '../components/Account/AccountModal';
import { Button } from '../components/Button';
import Header from '../components/Header';
import { useAccount } from '../hooks/useAccount';
import { useUser } from '../hooks/useUser';

const AccountsScreen = ({ navigation }: any) => {
	const { user } = useUser()
	const { openAccountModal } = useAccount()
	const [activeAccounts, setActiveAccounts] = useState(true)

	useEffect(() => {
		if (!user) {
			navigation.navigate('Login')
		}
	}, [user])

	return (
		<SafeAreaView style={styles.screen}>
			<AccountModal />

			<Header />
			<View style={styles.container}>
				<View style={styles.header}>
					<TouchableOpacity
						style={[styles.button, activeAccounts ? styles.activeButton : {}]}
						onPress={() => setActiveAccounts(true)}
					>
						<Text style={[styles.buttonText, activeAccounts ? styles.activeButtonText : {}]}>Contas Ativas</Text>
					</TouchableOpacity>

					<View style={{ width: 16 }} />

					<TouchableOpacity
						style={[styles.button, !activeAccounts ? styles.activeButton : {}]}
						onPress={() => setActiveAccounts(false)}
					>
						<Text style={[styles.buttonText, !activeAccounts ? styles.activeButtonText : {}]}>Contas Inativas</Text>
					</TouchableOpacity>
				</View>


				<AccountList active={activeAccounts} />

				<Button title='ADICIONAR CONTA' onPress={() => openAccountModal()} />
			</View>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		backgroundColor: '#2E3838',
	},
	container: {
		padding: 24,
		flex: 1,
	},
	header: {
		flexDirection: 'row',
		marginBottom: 40,
	},
	button: {
		padding: 8,
		borderWidth: 3,
		borderRadius: 8,
		borderColor: '#697070',
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttonText: {
		color: 'white',
		fontSize: 16,
		textTransform: 'uppercase',
	},
	activeButton: {
		backgroundColor: '#202727',
		borderColor: '#202727',
	},
	activeButtonText: {
		fontWeight: '700'
	}
});

export default AccountsScreen;