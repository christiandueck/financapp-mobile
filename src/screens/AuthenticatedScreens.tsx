import { createDrawerNavigator } from '@react-navigation/drawer';
import React from 'react';
import DrawerMenu from '../components/DrawerMenu';
import AccountsScreen from './AccountsScreen';
import CategoriesScreen from './CategoriesScreen';
import TransactionsScreen from './TransactionsScreen';

const Drawer = createDrawerNavigator();

const AuthenticatedScreen = () => {
	return (
		<Drawer.Navigator
			initialRouteName="Transactions"
			screenOptions={{
				drawerStyle: {
					backgroundColor: "#2E3838"
				},
				headerShown: false,
			}}
			drawerContent={DrawerMenu}
		>
			<Drawer.Screen name="Transactions" component={TransactionsScreen} />
			<Drawer.Screen name="Accounts" component={AccountsScreen} />
			<Drawer.Screen name="Categories" component={CategoriesScreen} />
		</Drawer.Navigator>
	)
}

export default AuthenticatedScreen;