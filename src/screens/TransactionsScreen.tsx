import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Button } from '../components/Button';
import Header from '../components/Header';
import { SelectMonth } from '../components/SelectMonth';
import { Summary } from '../components/Transaction/Summary';
import { TransactionList } from '../components/Transaction/TransactionList';
import { TransactionModal } from '../components/Transaction/TransactionModal';
import { useTransaction } from '../hooks/useTransaction';
import { useUser } from '../hooks/useUser';

const TransactionsScreen = ({ navigation }: any) => {
	const { user } = useUser()
	const { transactions, openTransactionModal } = useTransaction()
	const [filter, setFilter] = useState<null | 'income' | 'outcome'>(null)

	useEffect(() => {
		if (!user) {
			navigation.navigate('Login')
		}
	}, [user])

	return (
		<SafeAreaView style={styles.screen}>
			<TransactionModal />

			<Header />

			<View style={styles.container}>
				<SelectMonth />

				<View style={{ height: 20 }} />

				{transactions.length > 0 ? <>

					<Summary setFilter={setFilter} filter={filter} />

					<View style={{ height: 20 }} />

					<TransactionList filter={filter} />
				</>
					:
					<Text style={styles.text}>Nenhuma transação cadastrada.</Text>
				}

				<Button title="ADICIONAR TRANSAÇÃO" color="#319795" onPress={() => openTransactionModal()} />
			</View>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		backgroundColor: '#2E3838',
	},
	container: {
		padding: 24,
		flex: 1,
	},
	text: {
		color: 'white',
		textAlign: 'center',
		fontSize: 16,
		flex: 1,
	}
});

export default TransactionsScreen;