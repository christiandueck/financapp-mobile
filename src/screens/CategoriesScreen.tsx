import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Button } from '../components/Button';
import { CategoryList } from '../components/Category/CategoryList';
import { CategoryModal } from '../components/Category/CategoryModal';
import Header from '../components/Header';
import { useCategory } from '../hooks/useCategory';
import { useUser } from '../hooks/useUser';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { SelectTransactionType } from '../components/SelectTransactionType';

const CategoriesScreen = ({ navigation }: any) => {
	const { user } = useUser()
	const { openCategoryModal } = useCategory()
	const [activeCategories, setActiveCategories] = useState(true)
	const [type, setType] = useState<'income' | 'outcome'>('income')

	useEffect(() => {
		if (!user) {
			navigation.navigate('Login')
		}
	}, [user])

	return (
		<SafeAreaView style={styles.screen}>
			<CategoryModal />

			<Header />

			<View style={styles.container}>
				<SelectTransactionType type={type} setType={setType} />

				<View style={{ height: 20 }} />

				<TouchableOpacity
					style={styles.button}
					onPress={() => setActiveCategories(!activeCategories)}
				>
					<Text style={[styles.buttonText, styles.activeButtonText]}>
						{activeCategories ? 'Categorias Ativas' : 'Categorias Inativas'}
					</Text>

					<MaterialCommunityIcons name={activeCategories ? "toggle-switch" : "toggle-switch-off-outline"} size={32} color="white" />
				</TouchableOpacity>


				<CategoryList active={activeCategories} type={type} />

				<Button title='ADICIONAR CATEGORIA' onPress={() => openCategoryModal()} />
			</View>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		backgroundColor: '#2E3838',
	},
	container: {
		padding: 24,
		flex: 1,
	},
	button: {
		paddingVertical: 8,
		paddingHorizontal: 16,
		borderWidth: 3,
		borderRadius: 8,
		borderColor: '#697070',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		marginBottom: 20,
	},
	buttonText: {
		color: 'white',
		fontSize: 16,
		textTransform: 'uppercase',
	},
	activeButton: {
		backgroundColor: '#202727',
		borderColor: '#202727',
	},
	activeButtonText: {
		fontWeight: '700'
	}
});

export default CategoriesScreen;