# financapp-mobile
Aplicativo para gestão de finanças pessoais.
<br>O projeto foi desenvolvido para o TCC em Tecnologia em Análise de Desenvolvimento de Sistemas da Universidade Federal do Paraná, incialmente desenvolvido em Next.js para o ambiente web, com uma api em php desenvolvida por nós.

Aproveitamos o mesmo escopo e api para realizar o Trabalho da disciplina de <b>DS151 Desenvolvimento para Dispositivos Móveis</b>.

Aplicativo desenvolvido em React Native por <br><b>Christian Dueck</b>,
<br><b>Cleriton Adriano Muchinski</b>,
<br><b>Leonardo Castro de Oliveira</b>,

<br>

<hr>
<br>

## Getting started

Clone this repository and run following commands in the main project folder (you can use <b>yarn</b> or <b>npm</b>):

```
yarn
```
or
```
npm install
```

<p>To run the application with the <b>api</b>, you need to run the server</p> 
<p><i>project available at:</i>
<a href="https://github.com/leonardooliveira951/financapp-back" target="_blank">https://github.com/leonardooliveira951/financapp-back</a></p>

<br>
After cloning and setting up the <b>api server</b>,
you need to get the IP of the server (probably, the same IP expo uses) and change the <b>file: <code>"./src/services/api.ts"</code></b> with these IP, keeping <code><b>port 8000</b></code> as shown below:
<br>
<br>

```ts
export const api = axios.create({
	baseURL: 'http://192.168.0.14:8000/',
})
```

<br>

With the setup complete, you can run the project by running on the main folder of the project:

```
expo start
```